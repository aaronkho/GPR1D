core
====

baseclasses
-----------

.. automodule:: mkgp.core.baseclasses
   :members:
   :private-members:
   :undoc-members:
   :show-inheritance:

definitions
-----------

.. automodule:: mkgp.core.definitions
   :members:
   :undoc-members:
   :show-inheritance:

kernels
-------

.. automodule:: mkgp.core.kernels
   :members:
   :undoc-members:
   :show-inheritance:

routines
--------

.. automodule:: mkgp.core.routines
   :members:
   :undoc-members:
   :show-inheritance:

simple
------

.. automodule:: mkgp.core.simple
   :members:
   :undoc-members:
   :show-inheritance:

utils
-----

.. automodule:: mkgp.core.utils
   :members:
   :undoc-members:
   :show-inheritance:

