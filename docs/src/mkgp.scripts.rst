scripts
=======

demo
----

.. automodule:: mkgp.scripts.demo_1d
   :members: run_demo_1d
   :undoc-members:
   :show-inheritance:

profiler
--------

.. automodule:: mkgp.scripts.profiler
   :members: run_profiler
   :undoc-members:
   :show-inheritance:

