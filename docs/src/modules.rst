mkgp
====

.. automodule:: mkgp
   :members:

.. toctree::
   :maxdepth: 4

   mkgp.core
   mkgp.gui
   mkgp.scripts

